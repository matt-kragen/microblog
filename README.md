# Microblog Assessment Submission
>Thanks for the opportunity to join Wellfound!

## Intro

It's prudent to mention that I have not worked with Python in a professional setting, but I prefer it much more than the PHP-Laravel writing I do at my current job. Most of my experience is in Ruby, and I have plenty of experience with embedded code in templates. I consider myself to be language-agnostic when it comes to development, as I am still adding a lot of skills and experience to my portfolio. Shooting for the stars here!

## Solution

I noticed that `routes.py` contained a variable `posts` that could be called within the index page. Ergo, I opted for a simple solution that made use of the ready-made partial(i.e. `_post.html`) in the templates directory.

The original `index.html` code...
```html
<table class="table table-hover">
    <tr>
        <td>
            <br>
            <span id="post1">This is my one and only blog post. Can you add more using a for loop?</span>
        </td>
    </tr>
</table>
```

Was replaced with...

```html
{% for post in posts %}
    {% include '_post.html' %}
{% endfor %}
```

Apologies for the simplistic representation, but I couldn't spend too much time on this challenge.
Resulting HTML:

![image.png](./image.png)

As the partial contains `{{ post.body }}` within the same HTML template as the singular post in the original `index.html`, looping through `posts` should produce the requested list with unique IDs for each post.
